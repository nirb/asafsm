#!/bin/bash
# usage ./configWifi.sh countryCode ssid [ssid password]

#echo "number of args $#"

if (( $# < 2 )); then
	echo "Illegal number of parameters"
	echo "usage ./configWifi.sh countryCode ssid [ssid password]"
fi

countryCode=$1
countryCode=${countryCode^^} #to upper case
echo "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev" > network.txt
echo "update_config=1" >> network.txt
echo "country="$countryCode >> network.txt

if (( $# == 3 )); then
	wpa_passphrase $2 $3 >> network.txt
fi

if (( $# == 2 )); then
	echo "network={" >> network.txt
	echo "ssid=\""$2"\"" >> network.txt
	echo "key_mgmt=NONE" >> network.txt	
	echo "}" >> network.txt
fi

echo "writing wifi config"
cat network.txt
sudo cat network.txt > /etc/wpa_supplicant/wpa_supplicant.conf
