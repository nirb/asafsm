import subprocess
import sys

usbDevices = subprocess.check_output("lsusb").lower()

#print usbDevices
found = 0
if 'mouse' in usbDevices:
    print 'Mouse detected'
    found += 1

if 'keyboard' in usbDevices:
    print 'Keyboard detected'
    found += 1


if found >= 1:
    print 'Found keyboard and/or mouse'
    subprocess.call(['./../scripts/configGui.sh'])
    sys.exit()
else:
    # show version and boot info
    subprocess.call(['./../scripts/showBootInfo.sh'])
    # start the mirror
    subprocess.call(['./../scripts/startsm.sh'])
    print 'Mouse and Keyboard not detected'

