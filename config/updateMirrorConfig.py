import fileinput
import sys

if (len(sys.argv) != 4):
    print 'ERROR in program params'
    print 'USAGE - python updateMirrorConfig.py configFileName findString replaceString'

def replaceAll(file,searchExp,replaceExp):
    print file
    print searchExp
    print replaceExp
    output = ''
    f = open(file,'r')
    f.seek(0)
    lineNumber = 1
    print f
    for line in f:
        #print line
        if searchExp in line:
            #print line.index(searchExp)
            print 'lineNumber', lineNumber
            print 'oldLine', line
            space = 0
            tabs = 0
            for character in line:
                if character == " ":
                    space +=1
                elif character == '\t': 
                    tabs += 1

            #print 'spaces', space, 'tabs',tabs

            line = "";
            for i in range(0,space):
                line+=' '
            for i in range(0,tabs):
                line+='\t'

            line = line + searchExp + replaceExp + "\n"
            print 'newLine', line
        output+=line
        lineNumber+=1

    #print output
    f.close()

    f=open(file,'w')
    f.write(output)
    f.close()

replaceAll(sys.argv[1], sys.argv[2], sys.argv[3])
