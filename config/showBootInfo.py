from Tkinter import *
import subprocess
import tkMessageBox
import sys

version = '1.0.0'

elementWidth = 30
elementHeight = 30
        
root = Tk()

screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight() + 20

root.geometry(str(screen_width) + 'x' + str(screen_height) + '+00-20')
root.wm_title('Mirror Info')
#root.wm_attributes('-type', 'splash')
root.overrideredirect(1)

# open weather account
infoFrame = Frame(root)
infoFrame.pack()
infoLabel = Label(infoFrame, text="The Mirror will start in a few seconds...",width = elementWidth, height = elementHeight)
infoLabel.pack(side=BOTTOM)

versionLabel = Label(root, text='Mirror version - ' + version,width = elementWidth, height = elementHeight)
versionLabel.pack(side=BOTTOM)

#close the window after timeout
def close():
    close.timeOut -= 1
    if close.timeOut == 0:
        root.quit()
    root.after(1000, close)

close.timeOut = 30
if len(sys.argv) == 2:
    close.timeOut = int(sys.argv[1])

close()

root.mainloop()
