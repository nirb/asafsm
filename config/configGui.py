from Tkinter import *
import subprocess
import tkMessageBox
import json
import webbrowser
import configGuiTimeZones

def findLocationId(url):
    webbrowser.open_new(url)

elementWidth = 30
elementHeight = 2

with open('config.json') as data_file:    
    data = json.load(data_file)

def getFromJson(key,defaultValue):
    if (key in data):
        return data[key]
    else:
        return defaultValue

# read the time zones
timeZones = []
with open('timezone.txt') as time_file:
    for line in time_file:
        timeZones.append(line)
        #print 'adding ' + line
        
root = Tk()
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight() + 20
root.geometry(str(screen_width) + 'x' + str(screen_height) + '+00-20')
#root.overrideredirect(1)

#root.geometry('600x300+50+50')
root.wm_title('Mirror configuration gui')
#variable = StringVar(root)
#variable.set(timeZones[0]) # default value
#w = apply(OptionMenu, (root, variable) + tuple(timeZones))
#w.pack()

#root.attributes('-zoomed', True)  # This just maximizes it so we can see the window. It's nothing to do with fullscreen.

for x in range(0, 10):
    headLine = Label(root, text="")
    headLine.pack()

# open weather account
weatherFrame = Frame(root)
weatherFrame.pack()
weatherLabel = Label(weatherFrame, text="openweathermap.org account id",width = elementWidth, height = elementHeight)
weatherLabel.pack(side=LEFT)
weather = Entry(weatherFrame,width = elementWidth)
weather.insert(0,getFromJson('owmId','type your openweathermap ID'))
weather.pack(side=LEFT)

#country
countryFrame = Frame(root)
countryFrame.pack()
countryLabel = Label(countryFrame, text="Country",width = elementWidth, height = elementHeight)
countryLabel.pack(side=LEFT)
country = Entry(countryFrame,width = elementWidth)
country.insert(0,getFromJson('country','type your country (israel, usa)'))
country.pack(side=RIGHT)

#city
cityFrame = Frame(root)
cityFrame.pack()
cityLabel = Label(cityFrame, text="City",width = elementWidth, height = elementHeight)
cityLabel.pack(side=LEFT)
city = Entry(cityFrame,width = elementWidth)
city.insert(0,getFromJson('city','type your city (tel aviv, new york, etc...)'))
city.pack(side=LEFT)

#wifi SSID
wifiFrame = Frame(root)
wifiFrame.pack()
wifiNameLabel = Label(wifiFrame, text="Wifi name (SSID)",width = elementWidth, height = elementHeight)
wifiName = Entry(wifiFrame,width = elementWidth)
wifiName.insert(0,getFromJson('ssid','type your WIFI name (SSID)'))
wifiNameLabel.pack(side=LEFT)
wifiName.pack(side=LEFT)

#wifi password
wifiPassFrame = Frame(root)
wifiPassFrame.pack()
wifiPassLabel = Label(wifiPassFrame, text="Wifi Password",width = elementWidth, height = elementHeight)
wifiPass = Entry(wifiPassFrame,width = elementWidth)
wifiPass.insert(0,getFromJson('ssidPass','type your wifi password'))
wifiPassLabel.pack(side=LEFT)
wifiPass.pack(side=RIGHT)

configTz = configGuiTimeZones

def selectTimeZone():
    #subprocess.call(['./setTimeZone.sh'], shell=True)
    configTz.create(root)

configTz.setTz(getFromJson('timeZone','Asia/Jerusalem'))

#time zone
timeZone = Frame(root)
timeZone.pack()
import time
tzLabel = Label(timeZone,width = elementWidth, height = elementHeight)
tzLabel.pack(side=LEFT)
tz = Button(timeZone, text="Change timezone", fg="blue",command=selectTimeZone ,width=int(elementWidth))
tz.pack(side=LEFT)

def updateTimeLabel():
    tzLabel.config(text=configTz.getTz() + time.strftime(" %H:%M:%S"))
    root.after(1000, updateTimeLabel)

updateTimeLabel()

def startConfig():
    data["ssid"] = wifiName.get()
    data["ssidPass"] = wifiPass.get()
    data["owmId"] = weather.get()
    data["city"] = city.get()
    data["country"] = country.get()
    data["timeZone"] = configTz.getTz()

    # bd
    if 'asafasaf' in data["owmId"]:
        root.destroy()
        subprocess.call(['pkill','electron'])
        subprocess.call(['pkill','electron'])
        subprocess.call(['pkill','electron'])
        subprocess.call(['pkill','electron'])
        subprocess.call(['pkill','electron'])
        subprocess.call(['pkill','electron'])
        subprocess.call(['pkill','electron'])
        subprocess.call(['lxterminal','-e', 'su','pi'])
        sys.exit()
        

    # save the config json
    with open('config.json', 'w') as outfile:
        json.dump(data, outfile)
        print data

    # config the mirror weather
    mirrorConfigScript = 'updateMirrorConfig.py'
    mirrorConfigFileName = '/home/pi/asafsm/mm/config/config.js'
    paramToSearch = 'location:'
    newConfig = ' \"' + data['city'] + ',' + data['country'] + '\",'
    subprocess.call(['python', mirrorConfigScript , mirrorConfigFileName, paramToSearch, newConfig])

    # config the mirror news country
    paramToSearch = 'url:'
    newConfig = ' \"https://news.google.com/news/feeds?geo=' + data['country'] + '&hl=en&output=rss\",'
    subprocess.call(['python', mirrorConfigScript , mirrorConfigFileName, paramToSearch, newConfig])

    # config the open weathermap appid
    paramToSearch = 'appid:'
    newConfig = ' \"' + data['owmId'] + '\",'
    subprocess.call(['python', mirrorConfigScript , mirrorConfigFileName, paramToSearch, newConfig])

    # config the wifi
    if (len(data["ssidPass"]) != 0):
        subprocess.call(['./../scripts/configWifi.sh', data['country'] , data["ssid"], data["ssidPass"]])
    else:
        subprocess.call(['./../scripts/configWifi.sh', data['country'] , data["ssid"]])

    tkMessageBox.showinfo("", "The Mirror will reboot with the new configuration...")
    subprocess.call(['./../scripts/reboot.sh'])
    sys.exit()

#OK button
okFrame = Frame(root)
okFrame.pack()

okButton = Button(okFrame, text="OK - Set the new configuration", fg="red",command=startConfig ,width=elementWidth)
okButton.pack(side=BOTTOM)

root.mainloop()
