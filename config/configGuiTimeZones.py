from Tkinter import *
import subprocess
import tkMessageBox
import json

elementWidth = 6

def listBoxChange(evt):
    # Note here that Tkinter passes an event object to onselect()
    w = evt.widget
    index = int(w.curselection()[0])
    value = w.get(index)
    #print 'You selected item %d: "%s"' % (index, value)
    setTz.selectedTimeZone = value

def timeZoneSelected(root,father):
    # change the time zone
    subprocess.call(["sudo", "timedatectl", "set-timezone" , setTz.selectedTimeZone])
    root.destroy()
    father.destroy()

def userCancel(root, father):
    setTz.selectedTimeZone = setTz.originalTimeZone
    timeZoneSelected(root,father)    

def setTz(tz):
    setTz.selectedTimeZone = tz
    setTz.originalTimeZone = tz

def getTz():
    return setTz.selectedTimeZone

def createContinent(continentName, father):
    root = Toplevel(father)
    
    
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    root.geometry(str(int(screen_width*.75)) + 'x' + str(screen_height) + '+00+00')

    root.wm_title('Zimezone selection - ' + continentName)

    scrollbar = Scrollbar(root, width = 20)
    scrollbar.pack( side = RIGHT, fill=Y )
    lb = Listbox(root, yscrollcommand = scrollbar.set, width=30 )
    lb.bind('<<ListboxSelect>>', listBoxChange)

    #timeZones = []
    with open('timezone.txt') as f:
        for line in f:
            if continentName in line:
                #timeZones.append(line)
                lb.insert(END, line.rstrip())
    
    lb.pack( side = LEFT, fill = BOTH )
    scrollbar.config( command = lb.yview )

    f= Frame(root)
    f.pack()
    l = Label(f, text="Select City")
    l.pack(side=LEFT)

    f=Frame(root)
    f.pack()
    b = Button(f, text='Select', fg="blue",command=lambda r=root, f=father: timeZoneSelected(r,f) ,width=int(elementWidth))
    b.pack(side=LEFT)

    f=Frame(root)
    f.pack()
    b = Button(f, text='Cancel', fg="blue",command=lambda r=root, f=father: userCancel(r,f) ,width=int(elementWidth))
    b.pack(side=LEFT)



def create(mainWindow):
    root = Toplevel(mainWindow)
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    root.geometry(str(int(screen_width*.75)) + 'x' + str(screen_height) + '+00+00')
    root.wm_title('Zimezone selection')

    f= Frame(root)
    f.pack()
    l = Label(f, text="Select Continent")
    l.pack(side=LEFT)

    continents = ["Africa","America","Antarctica","Asia","Atlantic","Australia","Europe","Indian","Pacific"]

    f= Frame(root)
    f.pack()
    for continent in continents:
        b = Button(f, text=continent, fg="blue",command=lambda c=continent: createContinent(c,root) ,width=int(elementWidth))
        b.pack(side=LEFT)
